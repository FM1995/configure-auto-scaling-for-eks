# Configure Auto-Scaling for EKS

#### Project Outline

In this project, we will discover how we can implement auto-scaling in our EKS cluster to increase and decrease our nodes depending on the number of pods deployed and resources consumed and therefore optimise cost

## Lets get started

Can see the autoscaling group was also created

![Image1](https://gitlab.com/FM1995/configure-auto-scaling-for-eks/-/raw/main/Images/Image1.png)


There are few things we need to configure
Inorder for the autoscaling group to work, we need to create a policy that we need to give node group for the auto scaling to work
Lets create the policy

![Image2](https://gitlab.com/FM1995/configure-auto-scaling-for-eks/-/raw/main/Images/Image2.png)

Can see policy is now created

![Link 3 - Image3](https://gitlab.com/FM1995/configure-auto-scaling-for-eks/-/raw/main/Images/Image3.png)

Now lets attach the policy to the nodegroup IAM role

![Link 4 - Image4](https://gitlab.com/FM1995/configure-auto-scaling-for-eks/-/raw/main/Images/Image4.png)

Next thing we need to configure is the tags

And can see it has automatically been added to the auto-scaling group

![Link 5 - Image5](https://gitlab.com/FM1995/configure-auto-scaling-for-eks/-/raw/main/Images/Image5.png)

Now as a next step we can deploy the auto scaling component in to our cluster

Using the below 

Link: https://raw.githubusercontent.com/kubernetes/autoscaler/master/cluster-autoscaler/cloudprovider/aws/examples/cluster-autoscaler-autodiscover.yaml

We will do kubectl apply

![Link 6 - Image6](https://gitlab.com/FM1995/configure-auto-scaling-for-eks/-/raw/main/Images/Image6.png)

Now checking the deployment


```
kubectl get deployment -n kube-system cluster-autoscaler
```

![Link 7 - Image7](https://gitlab.com/FM1995/configure-auto-scaling-for-eks/-/raw/main/Images/Image7.png)

Now we need to a few changes to the deployment

```
kubectl edit deployment -n kube-system cluster-autoscaler
```

Adding the below

![Link 8 - Image8](https://gitlab.com/FM1995/configure-auto-scaling-for-eks/-/raw/main/Images/Image8.png)

Adding the cluster name and version and some further parameters

![Link 9 - Image9](https://gitlab.com/FM1995/configure-auto-scaling-for-eks/-/raw/main/Images/Image9.png)

Also using the below

https://github.com/kubernetes/autoscaler/tags

Can see the file got updated

![Link 10 - Image10](https://gitlab.com/FM1995/configure-auto-scaling-for-eks/-/raw/main/Images/Image10.png)

Lets now get the pod

```
kubectl get pod -n kube-system
```

![Link 11 - Image11](https://gitlab.com/FM1995/configure-auto-scaling-for-eks/-/raw/main/Images/Image11.png)

Can also see a few more details

![Link 12 - Image12](https://gitlab.com/FM1995/configure-auto-scaling-for-eks/-/raw/main/Images/Image12.png)

Aswell as the logs

```
kubectl logs -n kube-system cluster-autoscaler-5c9bf99d74-hm6kn
```

And can see scale down

![Link 13 - Image13](https://gitlab.com/FM1995/configure-auto-scaling-for-eks/-/raw/main/Images/Image13.png)

And testing it out, lets make the desired 1

![Link 14 - Image14](https://gitlab.com/FM1995/configure-auto-scaling-for-eks/-/raw/main/Images/Image14.png)

Can see it got scaled down to as opposed to two which we had before


![Link 15 - Image15](https://gitlab.com/FM1995/configure-auto-scaling-for-eks/-/raw/main/Images/Image15.png)

Lets now deploy an application in to our cluster and see how the nodes dynamically change their resources

In this part we will deploy an nginx application with a loadbalancer

```
kubectl apply -f nginx-deployment.yaml
```

![Link 16 - Image16](https://gitlab.com/FM1995/configure-auto-scaling-for-eks/-/raw/main/Images/Image16.png)

 
Can see deployment is successful aswell as the attachement of elb since we are connected to EKS


![Link 17 - Image17](https://gitlab.com/FM1995/configure-auto-scaling-for-eks/-/raw/main/Images/Image17.png)

Can see the created load balancer

![Link 18 - Image18](https://gitlab.com/FM1995/configure-auto-scaling-for-eks/-/raw/main/Images/Image18.png)

Lets now access on the below DNS

![Link 19 - Image19](https://gitlab.com/FM1995/configure-auto-scaling-for-eks/-/raw/main/Images/Image19.png)

Lets now increase the replica size to 20

Can see they are running

![Link 20 - Image20](https://gitlab.com/FM1995/configure-auto-scaling-for-eks/-/raw/main/Images/Image20.png)

Checking the logs to see if it has scaled up

```
kubectl logs -n kube-system cluster-autoscaler-5c9bf99d74-xmj6v | grep 'TriggeredScaleUp'
```

![Link 21 - Image21](https://gitlab.com/FM1995/configure-auto-scaling-for-eks/-/raw/main/Images/Image21.png)

Can then run the below

```
kubectl get nodes
```

![Link 22 - Image22](https://gitlab.com/FM1995/configure-auto-scaling-for-eks/-/raw/main/Images/Image22.png)







